require("bufferline").setup{
	options = {
		numbers = function(opts)
			return string.format("%s.", opts.ordinal)
		end,
    custom_filter = function(buf, buf_nums)
        -- dont show help buffers in the bufferline
        if vim.bo[buf].filetype == "help" then
					return true
				end

        -- you can use more custom logic for example
        -- don't show files matching a pattern
        if vim.fn.bufname(buf):match('[No Name]') then
        	return true
        end

				return false -- needed?
			end
		-- mode = 'tabs', -- groups stard here
		-- groups = {
		-- 	options = {
		-- 		toggle_hidden_on_enter = true -- when you re-enter a hidden group this options re-opens that group so the buffer is visible
		-- 	},
		-- 	items = {
		-- 		{
		-- 			name = "Tests", -- Mandatory
		-- 			-- highlight = {underline = true, sp = "blue"}, -- Optional
		-- 			auto_close = false,
		-- 			-- priority = 2, -- determines where it will appear relative to other groups (Optional)
		-- 			icon = "", -- Optional
		-- 			matcher = function(buf) -- Mandatory
		-- 				return buf.filename:match('%_test.go')
		-- 			end,
		-- 		},
		-- 		{
		-- 			name = "Others",
		-- 			highlight = {undercurl = true, sp = "green"},
		-- 			auto_close = false,  -- whether or not close this group if it doesn't contain the current buffer
		-- 			matcher = function(buf)
		-- 				return buf.filename:match('%_test.go')==false
		-- 			end,
		-- 			-- separator = { -- Optional
		-- 			-- 	style = require('bufferline.groups').separator.tab
		-- 			-- },
		-- 		}
		-- 	}
		-- } -- groups end here
	},
}


-- vim.keymap.set("n", "<silent><leader>1", ":lua require('bufferline').go_to(1, true)<CR>", {remap = false})
-- vim.keymap.set("n", "<silent><leader>2", ":lua require('bufferline').go_to(2, true)<CR>", {remap = false})
-- vim.keymap.set("n", "<silent><leader>3", ":lua require('bufferline').go_to(3, true)<CR>", {remap = false})
-- vim.keymap.set("n", "<silent><leader>4", ":lua require('bufferline').go_to(4, true)<CR>", {remap = false})
-- vim.keymap.set("n", "<silent><leader>5", ":lua require('bufferline').go_to(5, true)<CR>", {remap = false})
-- vim.keymap.set("n", "<silent><leader>6", ":lua require('bufferline').go_to(6, true)<CR>", {remap = false})
-- vim.keymap.set("n", "<silent><leader>7", ":lua require('bufferline').go_to(7, true)<CR>", {remap = false})
-- vim.keymap.set("n", "<silent><leader>8", ":lua require('bufferline').go_to(8, true)<CR>", {remap = false})
-- vim.keymap.set("n", "<silent><leader>9", ":lua require('bufferline').go_to(9, true)<CR>", {remap = false})
-- vim.keymap.set("n", "<silent><leader>$", ":lua require('bufferline').go_to(-1, true)<CR>", {remap = false})

vim.keymap.set("n", "<leader>1", "<cmd>BufferLineGoToBuffer 1<CR>", {remap = false})
vim.keymap.set("n", "<leader>2", "<cmd>BufferLineGoToBuffer 2<CR>", {remap = false})
vim.keymap.set("n", "<leader>3", "<cmd>BufferLineGoToBuffer 3<CR>", {remap = false})
vim.keymap.set("n", "<leader>4", "<cmd>BufferLineGoToBuffer 4<CR>", {remap = false})
vim.keymap.set("n", "<leader>5", "<cmd>BufferLineGoToBuffer 5<CR>", {remap = false})
vim.keymap.set("n", "<leader>6", "<cmd>BufferLineGoToBuffer 6<CR>", {remap = false})
vim.keymap.set("n", "<leader>7", "<cmd>BufferLineGoToBuffer 7<CR>", {remap = false})
vim.keymap.set("n", "<leader>8", "<cmd>BufferLineGoToBuffer 8<CR>", {remap = false})
vim.keymap.set("n", "<leader>9", "<cmd>BufferLineGoToBuffer 9<CR>", {remap = false})
vim.keymap.set("n", "<leader>$", "<cmd>BufferLineGoToBuffer -1<CR>", {remap = false})


