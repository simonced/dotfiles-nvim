
-- -----------------------------------------
-- aerial.nvim
-- -----------------------------------------
require('aerial').setup({
	layout = {
		default_direction = "float",
		max_width = {90, 0.9}, -- lesser of  60 cols or 50% of screen
	},
	keymaps = {
		["<esc>"] = "actions.close",
	},
	autojump = true,
	close_on_select = true,
})
vim.keymap.set('n', '<leader>fl', ':AerialToggle<cr>', {remap=false})

