local builtin = require('telescope.builtin')

require('telescope').setup{
	defaults = {
		layout_strategy = 'vertical',
		layout_config = { height = 0.95 },
	},
}

-- helper to search in config folder
function Vimfiles()
	-- folder of different system
	local folder = "~/.config/$NVIM_APPNAME"
	if package.config:sub(1,1) == '\\' then
		folder =  "~/Appdata/Local/$NVIM_APPNAME"
	end
	local opts = {
		cwd = folder,
		file_ignore_patterns = {'.git'},
		previewer = false,
		-- try to not show the whole path because I don't need it: none of below seems to work...
		shorten_path = true,
		path_display = {"smart"},
	}
	builtin.find_files(opts)
end
-- [e]dit [e]nvironment
vim.keymap.set("n","<leader>ee", ":lua Vimfiles()<CR>", {noremap=true, silent=true})

-- [f]ind [w]ord
vim.keymap.set('n', '<leader>fw', function()
	builtin.grep_string({ search = vim.fn.expand('<cword>') })
end,
{noremap = true})

-- search [f]iles [o]ld
vim.keymap.set('n', '<leader>fo', builtin.oldfiles, {noremap=true})

-- list [F]ile Git [C]hanges
vim.keymap.set('n', '<leader>fc', builtin.git_status, {noremap=true})

-- [w]orkspace [symbols] using lsp
vim.keymap.set('n', '<leader>ws', function()
	local q=vim.fn.input("Symbol partial name > ")
	builtin.lsp_workspace_symbols({query=q})
end, {noremap=true})

-- [w]orkspace [d]iagnostic
vim.keymap.set('n', '<leader>wd', builtin.diagnostics, {noremap=true})

-- [f]ind [f]ile
vim.keymap.set('n', '<leader>ff', builtin.find_files, {noremap=true})

-- search [f]iles in [g]it projects
vim.keymap.set('n', '<leader>fg', builtin.git_files, {noremap=true})

-- [g]rep [s]earch
vim.keymap.set('n', '<leader>gs', function()
	builtin.grep_string({ search = vim.fn.input("Grep > ") })
end,
{noremap=true})

-- open buffers list
vim.keymap.set('n', '<C-b>', builtin.buffers, {noremap=true})

-- select color scheme
vim.keymap.set('n', '<leader>cs', builtin.colorscheme, {noremap=true})

