
# SETUP

**BEFORE doing anything, move / backup any previous config!**

## Diclaimer

The main config is inspired by the video [0 to LSP : Neovim RC From Scratch](https://www.youtube.com/watch?v=w7i4amO_zaE&pp=ygUIMCB0byBsc3A%3D) from ThePrimeagen.  
This inspired me to get back to vim and redo my config with lua.  
Loving the experience, vim really adds fun to my daily job.

## Packer

The config relies on [Packer](https://github.com/wbthomason/packer.nvim), to install:

**PS** adapt to you environment (change `nvim` bellow by your config name if you use `$NVIM_APPNAME` (since nvim 0.9)).

For *nix:
```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

For Win:
```
git clone --depth 1 https://github.com/wbthomason/packer.nvim "$env:LOCALAPPDATA\nvim-data\site\pack\packer\start\packer.nvim"
```

## This conf

Do the same for that repo, adapt to your environment for the clone destination.

For *nix:
```
cd ~\.config
git clone ...
```

For Win:
```
cd ~\AppData\Local
git clone ...
```

A new folder with that config will be created, rename if needed.

# Usage

To launch nvim with a specific config (if not using default), one can do:
```
NVIM_APPNAME=xxx nvim
```

for Win, it's more like:
```
$env:NVIM_APPNAME=xxx & nvim
```

The main conf is in `lua/simonced `folder.

## Theme

One theme `rose-pine` can be used right away (in after/plugin/colors.lua).

Light:
```
:lua ColorMyPencilsLight()
```

Dark:
```
:lua ColorMyPencilsDark()
```

The functions above are tweaking some hl groups (remove italics in comments etc...).
Customize to your liking.

## Files

The main file exploration is with `Telescope` and the main mappings I use with it are in `after/plugin/telescope.lua`.  
One mapping I use all the time is `<leader>ee` to find a nvim config file and tweak it.

## Mappings + Options

Those are in `lua/simonced/remaps.lua` and `lua/simonced/options.lua` respectively.  
Those are my defaults, tweak as you need.  
I personnaly recomment trying `fj` and `jf` in insert mode as an escape key, it's very confortable, can't live without it.
