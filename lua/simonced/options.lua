-- ref: https://github.com/potamides/dotfiles/blob/master/.config/nvim/init.lua

-- display
vim.opt.showmode = false
vim.opt.showmatch = true
vim.opt.matchtime = 1
vim.opt.wildmode = "full"
vim.opt.wildmenu = true
vim.opt.scrolloff = 5
vim.opt.termguicolors = true

-- tabs are two spaces
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = false
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.shiftround = true

-- search related
vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.wrapscan = false

-- set list chars for horizontal scrolling
vim.opt.listchars:append{tab = "» ", precedes = "<", extends = ">"}
vim.opt.list = true

-- show line numbers and highlight cursor line number
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.cursorlineopt = "number"

-- mouse and clipboard integration
vim.opt.clipboard = "unnamedplus"
vim.opt.mouse = "a"

-- set an alternative layout that can be switched to in insert mode with CTRL-^
-- vim.opt.keymap = "kana" -- not using that until I know what id does...
vim.opt.iminsert = 0

