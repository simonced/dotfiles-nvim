-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
	-- Packer can manage itself
	use 'wbthomason/packer.nvim'

	-- telescope (fuzzy finder)
	use {
		'nvim-telescope/telescope.nvim', tag = '0.1.1',
		-- or                            , branch = '0.1.x',
		requires = { {'nvim-lua/plenary.nvim'} }
	}

	use 'nvim-pack/nvim-spectre'

	-- Harpoon (file switcher) from the man himself...
	use 'ThePrimeagen/harpoon'

	-- file explorer (replacement for ntrw)
	use {
		"nvim-neo-tree/neo-tree.nvim",
		branch = "v2.x",
		requires = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
			"MunifTanjim/nui.nvim",
		}
	}

	-- file outline
	use 'simrat39/symbols-outline.nvim'

	-- scrolling
	use 'psliwka/vim-smoothie'

	-- outline (file content)
	use 'stevearc/aerial.nvim'

	-- buffers in tab
	use {'akinsho/bufferline.nvim', tag = "*", requires = 'nvim-tree/nvim-web-devicons'}

	-- status line
	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'nvim-tree/nvim-web-devicons', opt = true }
	}

	-- color scheme
	use({ 'rose-pine/neovim', as = 'rose-pine' })
	use({ 'catppuccin/nvim', as = 'catppuccin' })
	use({ 'jnurmine/Zenburn' })
	use({ 'morhetz/gruvbox' })
	use({ 'EdenEast/nightfox.nvim', as = 'nightfox' })
	use({ 'kyoz/purify', rtp = 'vim', as = 'purify' })
	use({ 'NLKNguyen/papercolor-theme', as = 'papercolor' })
	use({ 'vim-scripts/Spacegray.vim', as = 'spacegray' })

	-- git tools
	-- use 'airblade/vim-gitgutter' -- buggy, with temp folder write errors and stuff
	use('mhinz/vim-signify', {run = ':SignifyEnableAll'})

	-- moving in the file
	use('justinmk/vim-sneak')

	-- format of code and syntax
	use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})

	-- emmet
	use('mattn/emmet-vim')

	-- comment
	use('terrortylor/nvim-comment')

	-- marks view in gutter
	use('chentoast/marks.nvim')
	-- use('kshenoy/vim-signature')

	-- vim surround, can't live without it!
	use('tpope/vim-surround')

	-- slp support : lsp-zero
	use {
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v2.x',
		requires = {
			-- LSP Support
			{'neovim/nvim-lspconfig'},             -- Required
			{                                      -- Optional
				'williamboman/mason.nvim',
				run =  ':MasonUpdate'
			},
			{'williamboman/mason-lspconfig.nvim'}, -- Optional

			-- Autocompletion
			{'hrsh7th/nvim-cmp'},     -- Required
			{'hrsh7th/cmp-nvim-lsp'}, -- Required
			{'L3MON4D3/LuaSnip'},     -- Required
		}
	}

	-- smarty support
	use "blueyed/smarty.vim"
end)
