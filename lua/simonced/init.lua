-- === INFO / start ===
-- to make all that work well on Windows, I had to fiddle around with:
-- - install llvm (choco install llvm) for compiling treesitter parsers for html and yaml
--   (ref: https://github.com/nvim-treesitter/nvim-treesitter/wiki/Windows-support#troubleshooting)
-- === INFO / end ===

-- force language into english
vim.cmd.lang("en_US")

require("simonced.options")
require("simonced.remap")
require("simonced.packer")
require("simonced.auto")
require("simonced.colors")

-- some abbreviations (should be in their own file...)
vim.cmd("iabbrev ve echo \"<pre>\"; print_r(); echo \"</pre>\"; // DBG<esc>F(a")
vim.cmd("iabbrev !! !=")
vim.cmd("iabbrev iferr if err!=nil {<CR>return nil, err<CR>}")

-- some custom highlights and bindings to navigate in project files
function Tmp()
	vim.cmd([[
hi MARKS guibg=#009900 guifg=Black
syntax match MARKS /.*ErrorContains.*/
nnoremap <DOWN> /ErrorContains<CR>
nnoremap <UP> ?ErrorContains<CR>
	]])
end

vim.keymap.set("n", "<leader>tmp", ":lua Tmp()<CR>", {remap=false})
