
-- trying to reset IME to romaji when we leave insertMode
-- spoiler alert: not working...
AugroupIME = vim.api.nvim_create_augroup("IMEreset", {clear = true})
vim.api.nvim_create_autocmd("InsertLeave", {
	command = "set iminsert=0",
	group = AugroupIME,
})

-- auto open location list after a :vim or :grep command
vim.api.nvim_exec([[
	augroup autolocationlist
		autocmd!
		autocmd QuickFixCmdPost [^l]* cwindow | redraw
	augroup END
]], false)


-- auto reload files when changed on disk
vim.api.nvim_exec([[
	set autoread
	augroup autoreload
		autocmd!
		autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * checktime
	augroup END
]], false)

-- auto format go files when saving
vim.api.nvim_exec([[
	augroup autogofmt
		autocmd!
		autocmd BufWritePost *.go silent! !go fmt %
	augroup END
]], false)

vim.api.nvim_exec([[
	augroup ced_foldstart
		autocmd!
		autocmd ColorScheme * highlight cedFoldStart guibg=#ff7272 guifg=black
		autocmd ColorScheme * highlight cedFoldEnd guibg=#ed9b68 guifg=black
		autocmd ColorScheme * highlight cedFoldPart guibg=#6e72f4 guifg=black
		autocmd ColorScheme * highlight cedTODO guibg=#ee0000 guifg=black gui=underdouble
		autocmd ColorScheme * highlight cedDBG guibg=#eeee00 guifg=black cterm=underdouble
		autocmd ColorScheme * highlight cedWIP guibg=#00ee00 guifg=black cterm=underdouble
		autocmd BufRead * syntax match cedFoldStart /\v.*\>\>\>.*/
		autocmd BufRead * syntax match cedFoldEnd /\v.*\<\<\<.*/
		autocmd BufRead * syntax match cedFoldPart /\v.*\=\=\=$/
		autocmd BufRead * syntax match cedTODO / TODO/
		autocmd BufRead * syntax match cedDBG / DBG/
		autocmd BufRead * syntax match cedWIP / WIP/
	augroup END
]], false)
