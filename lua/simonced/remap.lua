
-- spacebar as my leader
vim.g.mapleader = " "

-- remove some bindings
vim.keymap.set("n", "Q", "<nop>")

-- esc replacement in insert mode
vim.keymap.set("i","fj", "<esc>")
vim.keymap.set("i","jf", "<esc>")

vim.keymap.set("n","<leader><CR>", ":w<CR>")
vim.keymap.set("n","<leader>so", ":so<CR>")

-- toggle highlight search
vim.keymap.set("n", "<leader> ", ":set hlsearch!<CR>")
vim.keymap.set("n", "*", [[yiw:let @/="\\<".@"."\\>"<CR>:set hls<CR>]], {remap=false})

-- selection (indent and move)
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- window/split switching
vim.keymap.set("n", "<C-h>", "<C-w>h", {remap=false})
vim.keymap.set("n", "<C-j>", "<C-w>j", {remap=false})
vim.keymap.set("n", "<C-k>", "<C-w>k", {remap=false})
vim.keymap.set("n", "<C-l>", "<C-w>l", {remap=false})
-- jump between quick fix list items
vim.keymap.set('n', '[q', ':cprev<cr>', {remap=false})
vim.keymap.set('n', ']q', ':cnext<cr>', {remap=false})
-- open neotree and focus on current file
vim.keymap.set("n", "<leader>fn", ":NeoTreeRevealToggle<CR>", {remap=false})

-- sneak to characters
vim.keymap.set("n", "\\", [[<plug>Sneak_s]], {remap=false})
vim.keymap.set("n", "<leader>\\", [[<plug>Sneak_S]], {remap=false})

-- closing stuff:
-- buffer
vim.keymap.set("n","<leader>bb", ":bp|bd #<CR>")
-- window / frame
vim.keymap.set("n","<leader>ww", ":close<CR>")
-- quickfix window
vim.keymap.set("n","<leader>qq", ":cclose<CR>")

-- guiフォントを拡縮する
vim.keymap.set("n", "<C-8>", [[:lua Guifontscale(-1)<CR>]], { remap = false })
vim.keymap.set("n", "<C-9>", [[:lua Guifontscale(1)<CR>]], { remap = false })
function Guifontscale(n)
	if type(n) ~= "number" then
		return
	end

	local gfa = {}
	for c in vim.gsplit(vim.o.guifont, ":") do
		table.insert(gfa, c)
	end
	local buildnewgf = ""
	for k, v in ipairs(gfa) do
		if v:find("h", 1, true) == 1 then
			local fweight = ""
			for w in vim.gsplit(v, "h") do
				if tonumber(w) == nil then
						goto continue
				end
				local wn = tonumber(w)
				wn = wn + n
				fweight = fweight .. tostring(wn)
				::continue::
			end

			buildnewgf = buildnewgf .. "h" .. fweight .. ":"
		else
			-- v = string.gsub(v, " ", "_")
			buildnewgf = buildnewgf ..  v .. ":"
		end
	end

    -- local setcmd = "set guifont=" .. buildnewgf
	local setcmd = "Guifont! " .. buildnewgf
	vim.cmd(setcmd)
end

-- This works only for go files (for now)
function SwapImplTest()
	local currentFileName = vim.api.nvim_buf_get_name(0)
	local otherFile

	-- generate other file name
	if string.match(currentFileName, "_test") then
		-- open the non test file
		otherFile = string.gsub(currentFileName, "_test", "")
	else
		-- open the test file
		otherFile = string.gsub(currentFileName, ".go", "_test.go")
	end

	-- open file if it exists
	local f = io.open(otherFile, "r")
	if f ~= nil then
		io.close(f)
		vim.cmd("e " .. otherFile)
	else
		print("Other file doesn't exist")
	end
end
vim.keymap.set("n", "<leader>tt", ":lua SwapImplTest()<CR>", {remap = false})
