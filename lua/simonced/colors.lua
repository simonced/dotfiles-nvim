
function RemoveCommentItalics()
	local comment = vim.api.nvim_get_hl(0, {name="Comment"})
	local newComment = {fg=comment['fg']}
	vim.api.nvim_set_hl(0, "Comment", newComment)
end


function CustomHl()
	-- active tab bg color
	vim.api.nvim_set_hl(0, "TabLineSel", {bg="#6b69d6", fg="#ffffff"})
end


-- AugroupCHL = vim.api.nvim_create_augroup("CustomHL", {clear = true})
-- vim.api.nvim_create_autocmd("ColorScheme", {
-- 	callback = function()
-- 		-- clears italics in comments
-- 		RemoveCommentItalics()
-- 		-- apply my "tags" syntax highlight
-- 		CustomHl()
-- 	end,
-- 	group = AugroupCHL,
-- })

-- @param string colorscheme name
-- @param string variant `light` or `dark`
local function ColorMyPencils(color, variant)
	-- defaults
	color = color or "dayfox"
	variant = variant or "light"
	-- vim setup
	vim.opt.termguicolors = true
	vim.opt.background = variant
	vim.cmd.colorscheme(color)
end

-- Dark color scheme
function ColorMyPencilsDark(color)
	color = color or "nightfox"
	ColorMyPencils(color, "dark")
	-- === dark custom ===
	-- custom hl for indent and space markers
	if(color=="zenburn") then
		vim.api.nvim_set_hl(0, "NonText", {fg="#656565"})
	elseif(color=="gruvbox") then
		vim.api.nvim_set_hl(0, "NonText", {fg="#4e4e4e"})
	end
end

-- Light version
function ColorMyPencilsLight(color)
	color = color or "dayfox"
	ColorMyPencils(color, "light")
	-- === light custom ===
	-- custom hl for indent and space markers
	vim.api.nvim_set_hl(0, "NonText", {fg="#e6e0da"})
end

